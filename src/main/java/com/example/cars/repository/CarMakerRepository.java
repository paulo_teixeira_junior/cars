package com.example.cars.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.cars.model.CarMaker;

@Repository
public interface CarMakerRepository extends JpaRepository<CarMaker, Long> {

	CarMaker findByName(String name);

}
