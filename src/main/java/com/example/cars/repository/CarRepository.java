package com.example.cars.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import com.example.cars.model.Car;


@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

	@EntityGraph(value = "carmakers.description", type = EntityGraphType.LOAD)
	Car getById(@PathVariable Long id);
	
}
