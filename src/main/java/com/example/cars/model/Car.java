package com.example.cars.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;

@Entity
@NamedEntityGraph(name = "carmakers.description", attributeNodes = @NamedAttributeNode(value = "carMaker"))
public class Car {

	public Car() {
		
	}
	
	public Car(Long id, String name, Double potency, CarMaker carMaker) {
		this.id = id;
		this.name = name;
		this.potency = potency;
		this.carMaker = carMaker;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private Double potency;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE })
	@JoinColumn(name = "carMakerId",  referencedColumnName = "id", nullable = false)
	private CarMaker carMaker;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPotency() {
		return potency;
	}

	public void setPotency(Double potency) {
		this.potency = potency;
	}

	public CarMaker getCarMaker() {
		return carMaker;
	}

	public void setCarMaker(CarMaker carMaker) {
		this.carMaker = carMaker;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carMaker == null) ? 0 : carMaker.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((potency == null) ? 0 : potency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (carMaker == null) {
			if (other.carMaker != null)
				return false;
		} else if (!carMaker.equals(other.carMaker))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (potency == null) {
			if (other.potency != null)
				return false;
		} else if (!potency.equals(other.potency))
			return false;
		return true;
	}
}
