package com.example.cars.service.impl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.cars.model.Car;
import com.example.cars.model.CarMaker;
import com.example.cars.repository.CarMakerRepository;
import com.example.cars.repository.CarRepository;
import com.example.cars.service.CarService;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarRepository carRepository;
	
	@Autowired
	private CarMakerRepository carMakerRepository;
	
	@Override
	public List<Car> getAll(@RequestParam boolean sortByName) {
		List<Car> listCar = carRepository.findAll();
		if(sortByName) {
			listCar = listCar.stream().sorted((c1, c2) -> 
				c1.getName().compareTo(c2.getName())).collect(Collectors.toList());
		}
		
		return listCar;
	}

	@Override
	public Car getCar(Long id) {
		return carRepository.findById(id).orElse(null);
	}

	@Override
	public void save(Car car) {
		CarMaker entity = carMakerRepository.findByName(car.getCarMaker().getName());
		if(entity != null) {
			car.setCarMaker(entity);
		}
		carRepository.save(car);
	}

	@Override
	public void update(Long id, Car car) {
		Car entity = carRepository.findById(id).orElse(null);
		if(entity != null) {
			car = entity;
			car.setId(id);
			carRepository.save(car);
		}
	}

	@Override
	public void delete(Long id) {
		carRepository.deleteById(id);
	}

}
