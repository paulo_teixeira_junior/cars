package com.example.cars.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.cars.model.CarMaker;
import com.example.cars.repository.CarMakerRepository;
import com.example.cars.service.CarMakerService;

@Service
public class CarMakerServiceImpl implements CarMakerService {

	@Autowired
	CarMakerRepository carMakerRepository;
	
	@Override
	public List<CarMaker> getAll(boolean sortByName) {
		List<CarMaker> listCarMaker = carMakerRepository.findAll();
		if(sortByName) {
			listCarMaker = listCarMaker.stream().sorted((c1, c2) ->
				c1.getName().compareTo(c2.getName())).collect(Collectors.toList());
		}
		
		return listCarMaker; 
	}

	@Override
	public CarMaker getCarMaker(Long id) {
		return carMakerRepository.findById(id).orElse(null);
	}

	@Override
	public void save(CarMaker carMaker) {
		carMakerRepository.save(carMaker);
	}

	@Override
	public void update(Long id, CarMaker carMaker) {
		CarMaker entity = carMakerRepository.findById(id).orElse(null);
		if(entity != null) {
			entity = carMaker;
			entity.setId(id);
			entity.getCars().stream().forEach(c -> c.setCarMaker(carMaker));
			carMakerRepository.save(entity);
		}
	}

	@Override
	public void delete(Long id) {
		carMakerRepository.deleteById(id);
	}

}
