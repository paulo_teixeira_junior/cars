package com.example.cars.service;

import java.util.List;

import com.example.cars.model.CarMaker;

public interface CarMakerService {
	
	List<CarMaker> getAll(boolean sortByName);
	
	CarMaker getCarMaker(Long id);
	
	void save(CarMaker carMaker);
	
	void update(Long id, CarMaker carMaker);
	
	void delete(Long id);
}
