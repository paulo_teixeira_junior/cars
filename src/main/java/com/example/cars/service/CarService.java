package com.example.cars.service;

import java.util.List;

import com.example.cars.model.Car;

public interface CarService {

	List<Car> getAll(boolean sortByName);
	
	Car getCar(Long id);
	
	void save(Car car);
	
	void update(Long id, Car car);
	
	void delete(Long id);
}
