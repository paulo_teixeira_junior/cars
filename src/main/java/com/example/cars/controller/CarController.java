package com.example.cars.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cars.model.Car;
import com.example.cars.service.CarService;

@RestController
@RequestMapping("its")
public class CarController {

	@Autowired
	private CarService carService;
	
	@GetMapping("/cars")
	public List<Car> getAll(@RequestParam(defaultValue = "false", required = false, name = "sortByName") boolean sortByName) {
		return carService.getAll(sortByName);
	}
	
	@GetMapping("/cars/{id}")
	public Car getCar(@PathVariable Long id) {
		return carService.getCar(id);
	}

	@PostMapping("/cars")
	public void saveCar(@RequestBody Car car) {
		carService.save(car);
	}
	
	@DeleteMapping("/cars/{id}")
	public void deleteCar(@PathVariable Long id) {
		carService.delete(id);
	}

	@PatchMapping("/cars/{id}")
	public void updateCar(@PathVariable Long id, @RequestBody Car car) {
		carService.update(id, car);
	}
}
