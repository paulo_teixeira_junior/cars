package com.example.cars.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cars.model.CarMaker;
import com.example.cars.service.CarMakerService;

@RestController
@RequestMapping("its")
public class CarMakerController {

	@Autowired
	private CarMakerService carMakerService;
	
	@GetMapping("/carmakers")
	public List<CarMaker> getAll(@RequestParam(defaultValue = "false", name = "sortByName", required = false) boolean sortByName){
		return carMakerService.getAll(sortByName);
	}

	@GetMapping("/carmakers/{id}")
	public CarMaker getCarMaker(@PathVariable Long id) {
		return carMakerService.getCarMaker(id);
	}
	
	@PostMapping("/carmakers")
	public void saveCarMaker(@RequestBody CarMaker carMaker) {
		carMakerService.save(carMaker);
	}
	
	@DeleteMapping("/carmakers/{id}")
	public void deleteCarMaker(@PathVariable Long id) {
		carMakerService.delete(id);
	}
	
	@PatchMapping("/carmakers/{id}")
	public void update(@PathVariable Long id, @RequestBody CarMaker carMaker) {
		carMakerService.update(id, carMaker);
	}
}
